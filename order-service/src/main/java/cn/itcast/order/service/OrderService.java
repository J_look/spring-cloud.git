package cn.itcast.order.service;

import cn.itcast.order.mapper.OrderMapper;
import cn.itcast.order.pojo.Order;
import cn.itcast.order.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class OrderService {

    @Autowired
    private OrderMapper orderMapper;

    @Autowired
    private RestTemplate restTemplate;
    public Order queryOrderById(Long orderId) {
        // 1.查询订单
        Order order = orderMapper.findById(orderId);
        // url 查询用户的接口
        String url="http://userservice/user/"+order.getUserId();
        // 2. 通过订单对象 获取用户id 发送请求  实现远程调用
        User user = restTemplate.getForObject(url, User.class);
        // 3 将接口返回的对象注入订单中
        order.setUser(user);
        // 4.返回
        return order;
    }
}

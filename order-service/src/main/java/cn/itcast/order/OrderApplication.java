package cn.itcast.order;

import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RandomRule;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@MapperScan("cn.itcast.order.mapper")
@SpringBootApplication
public class OrderApplication {

    public static void main(String[] args) {
        SpringApplication.run(OrderApplication.class, args);
    }


    /**
     * 发送请求的对象
     * @return
     */
    @Bean
    @LoadBalanced// 负载均衡
    public  RestTemplate restTemplate() {
        return new RestTemplate();
    }

    /**
     * 更改负载均衡的策略
     */
    @Bean
    public IRule RandomRule(){
        return new RandomRule();
    }


}